FROM nginx:1.15.5-alpine

RUN rm -fvr /etc/nginx/*
COPY --chown=root:root etc/nginx/ /etc/nginx/
RUN ln -s /usr/lib/nginx/modules /etc/nginx/

